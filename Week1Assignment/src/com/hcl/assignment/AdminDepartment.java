package com.hcl.assignment;

	public class AdminDepartment extends SuperDepartment{

			//declare method departmentName of return type string
			public String departmentName() {
			return "ADMIN DEPARTMENT";
			}


		//declare method getTodaysWork of return type string
		public String getTodaysWork() {
		return "COMPLETE YOUR DOCUMENTS SUBMISSION";
		}

		//declare method getWorkDeadline of return type string
		public String getWorkDeadline() {
		return "COMPLETE BY END OF THE DAY ";
		}

		}
