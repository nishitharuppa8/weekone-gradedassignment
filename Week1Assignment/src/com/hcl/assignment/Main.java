package com.hcl.assignment;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SuperDepartment sd = new SuperDepartment();
		AdminDepartment ad = new AdminDepartment();
		HrDepartment hd = new HrDepartment();
		TechDepartment td = new TechDepartment();
		
		System.out.println();
		System.out.println(sd.departmentName());
		System.out.println(sd.getTodaysWork());
		System.out.println(sd.getWorkDeadline());
		System.out.println(sd.isTodayAHoliday());
		System.out.println();
		
		System.out.println(ad.departmentName());
		System.out.println(ad.getTodaysWork());
		System.out.println(ad.getWorkDeadline());
		
		System.out.println();
		
		System.out.println(hd.departmentName());
		System.out.println(hd.getTodaysWork());
		System.out.println(hd.getWorkDeadline());
		System.out.println(hd.doactivity());
	
		 
		System.out.println();
		
		System.out.println(td.departmentName());
		System.out.println(td.getTodaysWork());
		System.out.println(td.getWorkDeadline());
		System.out.println(td.getTechStackInformation());
		 
	}

}

